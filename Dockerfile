FROM maven:3.8.4-openjdk-17-slim AS builder

WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY src ./src

RUN mvn package

WORKDIR /app

EXPOSE 3001

CMD ["java", "-jar", "app.jar"]

RUN mvn clean package
