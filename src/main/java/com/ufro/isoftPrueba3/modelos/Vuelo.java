package com.ufro.isoftPrueba3.modelos;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Vuelo {

    @Id
    private String id;
    private String origen;
    private String destino;
    private String fechaVuelo;
    private String estadoVuelo;

}
