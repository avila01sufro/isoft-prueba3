package com.ufro.isoftPrueba3.modelos;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
public class Pasajero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String correo;
    private String numeroTelefono;
    private Date fechaUltimoVuelo;
    @ManyToMany
    @JoinTable(
            name = "Pasajero_Vuelo",
            joinColumns = @JoinColumn(name = "pasajero_id"),
            inverseJoinColumns = @JoinColumn(name = "vuelo_id")
    )
    private Set<Vuelo> vuelosComprados;
}
