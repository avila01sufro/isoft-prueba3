package com.ufro.isoftPrueba3.controladores;

import com.ufro.isoftPrueba3.servicios.EstadisticasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/estadisticas")
public class EstadisticasController {
    @Autowired
    private EstadisticasService estadisticasService;

    @PostMapping("/findPassengersByYear")
    public List<String> findPassengersByYear(@RequestBody Map<String, String> jsonInput) {
        String yearInput = jsonInput.get("yearInput");
        return estadisticasService.findPassengersByYear(yearInput);
    }

    @GetMapping("/cancelledFlights")
    public List<String> analyzeCancelledFlights() {
        return estadisticasService.analyzeCancelledFlights();
    }

    @GetMapping("/frequentDestinations")
    public List<String> analyzeFrequentDestinations() {
        return estadisticasService.analyzeFrequentDestinations();
    }
}
