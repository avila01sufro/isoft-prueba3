package com.ufro.isoftPrueba3.controladores;

import com.ufro.isoftPrueba3.modelos.Pasajero;
import com.ufro.isoftPrueba3.modelos.PasajeroReqDestino;
import com.ufro.isoftPrueba3.servicios.PasajeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pasajero")
public class PasajeroController {

    @Autowired
    PasajeroService pasajeroService;

    @GetMapping("/pendiente")
    private String getPasajeroVueloPendiente(){
        return pasajeroService.getPasajeroVueloFuturo();
    }

    @GetMapping("/cancelados")
    private String getPasajeroMitadCancelados (){
        return pasajeroService.getPasajeroCancelados();
    }

    @GetMapping("/pordestino")
    private String getPasajeroDestino (@RequestBody PasajeroReqDestino APIRequest){
        return pasajeroService.getPasajeroDestino(APIRequest);
    }



}
