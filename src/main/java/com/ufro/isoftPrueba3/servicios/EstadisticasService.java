package com.ufro.isoftPrueba3.servicios;

import com.ufro.isoftPrueba3.modelos.Vuelo;
import com.ufro.isoftPrueba3.repositorios.PasajeroRepository;
import com.ufro.isoftPrueba3.repositorios.VueloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class EstadisticasService {
    @Autowired
    private VueloRepository vueloRepository;

    @Autowired
    private PasajeroRepository pasajeroRepository;

    private String vuelosFilePath = "src/main/dataset/vuelos.csv";
    private String pasajerosFilePath = "src/main/dataset/pasajeros.csv";
    private SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
    private Date currentDate = new Date();


    public List<String> findPassengersByYear(String yearInput) {
        List<String> passengersWithFlights = new ArrayList<>();
        passengersWithFlights.add("### INICIO PASAJEROS CON VUELOS A " + yearInput + " ###");

        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            BufferedReader pasajerosReader = new BufferedReader(new FileReader(pasajerosFilePath, StandardCharsets.UTF_8));

            String vuelosLine;
            String pasajerosLine;

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");
                String fechaVueloStr = vuelosData[3];

                try {
                    Date fechaVuelo = dateFormat.parse(fechaVueloStr);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(fechaVuelo);
                    int year = calendar.get(Calendar.YEAR);

                    if (String.valueOf(year).equals(yearInput)) {
                        String vueloId = vuelosData[0];

                        while ((pasajerosLine = pasajerosReader.readLine()) != null) {
                            String[] pasajerosData = pasajerosLine.split(";");
                            String vuelosComprados = pasajerosData[4];

                            if (vuelosComprados.contains(vueloId)) {
                                String passengerInfo = pasajerosData[0] + ";" + vuelosData[3] + ";" + vueloId;
                                passengersWithFlights.add(passengerInfo);
                            }
                        }
                        pasajerosReader.close();
                        pasajerosReader = new BufferedReader(new FileReader(pasajerosFilePath, StandardCharsets.UTF_8));
                    }
                } catch (ParseException e) {
                    System.err.println("Error analizando la fecha: " + fechaVueloStr);
                }
            }

            vuelosReader.close();
            pasajerosReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        passengersWithFlights.add("### FIN PASAJEROS CON VUELOS A " + yearInput + " ###");

        return passengersWithFlights;
    }








    public List<String> analyzeCancelledFlights() {
        List<String> cancelledFlights = new ArrayList<>();
        cancelledFlights.add("## INICIO VUELOS FUTUROS CANCELADOS ###");


        Date today = new Date();

        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            String vuelosLine;

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");
                String fechaVueloStr = vuelosData[3];
                String estadoVuelo = vuelosData[5];

                if ("Cancelado".equals(estadoVuelo)) {
                    try {
                        Date fechaVuelo = dateFormat.parse(fechaVueloStr);

                        if (fechaVuelo.after(today)) {
                            String vueloInfo = vuelosData[0] + ";" + fechaVueloStr + ";" + vuelosData[1] + ";" + vuelosData[2];
                            cancelledFlights.add(vueloInfo);
                        }
                    } catch (ParseException e) {
                        System.err.println("Error analizando la fecha: " + fechaVueloStr);
                    }
                }
            }

            vuelosReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cancelledFlights.add("### FIN VUELOS FUTUROS CANCELADOS ###");

        return cancelledFlights;
    }






    public List<String> analyzeFrequentDestinations() {
        List<String> frequentDestinations = new ArrayList<>();
        frequentDestinations.add("### INICIO TOP 5 DESTINOS ###");

        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            String vuelosLine;
            Map<String, Integer> destinosFrecuentes = new HashMap<>();

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");
                String destino = vuelosData[2];

                destinosFrecuentes.put(destino, destinosFrecuentes.getOrDefault(destino, 0) + 1);
            }

            vuelosReader.close();

            PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
            pq.addAll(destinosFrecuentes.entrySet());

            int count = 0;
            while (!pq.isEmpty() && count < 5) {
                Map.Entry<String, Integer> entry = pq.poll();
                frequentDestinations.add(entry.getKey() + " - Frecuencia: " + entry.getValue());
                count++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        frequentDestinations.add("### FIN TOP 5 DESTINOS ###");

        return frequentDestinations;
    }

}
