package com.ufro.isoftPrueba3.servicios;

import com.opencsv.CSVReader;
import com.ufro.isoftPrueba3.modelos.Pasajero;
import com.ufro.isoftPrueba3.modelos.PasajeroReqDestino;
import org.springframework.stereotype.Service;
import java.time.temporal.ChronoUnit;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Stack;

@Service
public class PasajeroService {


    private Pasajero pasajero;
    String pasajerosFilePath = "src/main/data/pasajeros.csv";
    String vuelosFilePath = "src/main/data/vuelos.csv";

    public String getPasajeroVueloFuturo(){

        String resultado = "### INICIO PASAJEROS CON VUELOS FUTUROS ###\n";

        try {
            // Leer el archivo de vuelos y obtener la fecha actual
            Date currentDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");

            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            BufferedReader pasajerosReader = new BufferedReader(new FileReader(pasajerosFilePath, StandardCharsets.UTF_8));

            String vuelosLine;
            String pasajerosLine;

            List<String> idVuelosFuturos = new ArrayList<>();

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");

                // Asegúrate de que la fecha esté en el índice correcto
                String fechaVueloStr = vuelosData[3];
                Date fechaVuelo = null;

                try {
                    fechaVuelo = dateFormat.parse(fechaVueloStr);
                } catch (ParseException e) {
                    // Manejar el error si la fecha no se puede analizar
                    System.err.println("Error analizando la fecha: " + fechaVueloStr);
                }

                if (fechaVuelo != null && fechaVuelo.after(currentDate)) {

                    idVuelosFuturos.add(vuelosData[0]) ;

                }
            }



            while ((pasajerosLine = pasajerosReader.readLine()) != null) {
                String[] pasajerosData = pasajerosLine.split(";");
                String[] vuelosIndividuales = pasajerosData[4].split("\\|");

                for (String idVueloPasajero : vuelosIndividuales) {
                    for (String idVueloFuturo : idVuelosFuturos) {
                        if(idVueloPasajero.equals(idVueloFuturo)){
                            resultado = resultado.concat(pasajerosData[0]+";"+pasajerosData[1]+";"+pasajerosData[3]+";"+getStatusVuelo(idVueloFuturo)+"\n");
                        }

                    }
                }
            }

            resultado = resultado.concat("### FIN PASAJEROS CON VUELOS FUTUROS ###\n");

            System.out.println("IDs:");
            for(String id : idVuelosFuturos){
                System.out.println(id);
            }

            vuelosReader.close();
        } catch (Exception e) {
            System.out.println("Se murió");
        }



        return resultado;
    }

    public String getPasajeroCancelados(){

        String resultado = "### INICIO PASAJEROS CON VUELOS CANCELADOS ###\n";

        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            BufferedReader pasajerosReader = new BufferedReader(new FileReader(pasajerosFilePath, StandardCharsets.UTF_8));

            String vuelosLine;
            String pasajerosLine;

            List<String> idVuelosCancelados = new ArrayList<>();

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");

                // Asegúrate de que la fecha esté en el índice correcto
                String estadoVuelo = vuelosData[5];


                if (estadoVuelo.equals("Cancelado")) {
                    idVuelosCancelados.add(vuelosData[0]) ;
                }
            }



            while ((pasajerosLine = pasajerosReader.readLine()) != null) {
                String[] pasajerosData = pasajerosLine.split(";");
                String[] vuelosIndividuales = pasajerosData[4].split("\\|");

                for (String idVueloPasajero : vuelosIndividuales) {
                    for (String idVueloFuturo : idVuelosCancelados) {
                        if(idVueloPasajero.equals(idVueloFuturo)){
                            resultado = resultado.concat(pasajerosData[0]+";"+pasajerosData[3]+";"+idVueloPasajero+"\n");
                        }

                    }
                }
            }

            resultado = resultado.concat("### FIN PASAJEROS CON VUELOS CANCELADOS ###\n");

            vuelosReader.close();
        } catch (Exception e) {
            System.out.println("Se murió");
        }



        return resultado;
    }

    public String getPasajeroDestino(PasajeroReqDestino req){


        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
        String destino = req.getDestino().trim();

        System.out.println(destino);
        String resultado = "### INICIO PASAJEROS CON VUELOS A "+destino+" ###\n";

        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            BufferedReader pasajerosReader = new BufferedReader(new FileReader(pasajerosFilePath, StandardCharsets.UTF_8));

            String vuelosLine;
            String pasajerosLine;

            List<String> idVuelosSimilares = new ArrayList<>();

            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");

                String destinoVuelo = vuelosData[2].trim();

                if (destinoVuelo.equals(destino)) {
                    idVuelosSimilares.add(vuelosData[0]) ;
                }
            }

            Date fechaVueloParam = getFechaVuelo(idVuelosSimilares.get(0));


            while ((pasajerosLine = pasajerosReader.readLine()) != null) {
                String[] pasajerosData = pasajerosLine.split(";");
                String[] vuelosIndividuales = pasajerosData[4].split("\\|");

                for (String idVueloPasajero : vuelosIndividuales) {
                    for (String idVueloFuturo : idVuelosSimilares) {
                        if(idVueloPasajero.equals(idVueloFuturo)){
                            if(isClosestVuelo(vuelosIndividuales, fechaVueloParam)) {
                                isClosestVuelo(vuelosIndividuales, fechaVueloParam);
                                resultado = resultado.concat(pasajerosData[0] + ";" + pasajerosData[3] + ";" + idVueloPasajero + "\n");
                            }
                        }
                    }
                }
            }

            resultado = resultado.concat("### FIN PASAJEROS CON VUELOS a "+destino+ "###\n");

            vuelosReader.close();
        } catch (Exception e) {
            System.out.println("Se murió");
        }



        return resultado;
    }

    private boolean isClosestVuelo(String[] vuelosIndividuales, Date fechaVuelo) {
        boolean isClosest = true;

        for (String idVuelo : vuelosIndividuales) {
            Date fechaComparar= getFechaVuelo(idVuelo);
            if(isDateForACloserThanB(fechaVuelo, getFechaVuelo(idVuelo))){
                isClosest = false;
            }
        }

        return isClosest;
    }

    private String getStatusVuelo(String idVuelo){
        String status = "No existe";
        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            String vuelosLine;
            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");

                    if (vuelosData[0].equals(idVuelo)){
                        status = vuelosData[5];
                    }
                }
            } catch (Exception e) { System.out.println("File not found"); }

            return status;
    }

    private Date getFechaVuelo(String idVuelo){
        Date dateVuelo = new Date();
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");


        try {
            BufferedReader vuelosReader = new BufferedReader(new FileReader(vuelosFilePath, StandardCharsets.UTF_8));
            String vuelosLine;
            while ((vuelosLine = vuelosReader.readLine()) != null) {
                String[] vuelosData = vuelosLine.split(";");

                if (vuelosData[0].equals(idVuelo)){

                    while ((vuelosLine = vuelosReader.readLine()) != null) {
                        String fechaVueloStr = vuelosData[3];
                        Date fechaVuelo = null;

                        try {
                            fechaVuelo = dateFormat.parse(fechaVueloStr);
                            dateVuelo = fechaVuelo;
                        } catch (ParseException e) {
                            // Manejar el error si la fecha no se puede analizar
                            System.err.println("Error analizando la fecha: " + fechaVueloStr);
                        }
                    }

                }
            }
        } catch (Exception e) { System.out.println("File not found"); }

        return dateVuelo;
    }


    private boolean isDateForACloserThanB(Date A, Date B){
        Date piss = new Date();

        LocalDate newA = A.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate newB = A.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentDate = LocalDate.now();

        long daysDifferenceA = ChronoUnit.DAYS.between(newA, currentDate);
        long daysDifferenceB = ChronoUnit.DAYS.between(newB, currentDate);

        if(daysDifferenceA < daysDifferenceB){
            return true;
        } else return false;
    }


}
