package com.ufro.isoftPrueba3.repositorios;

import com.ufro.isoftPrueba3.modelos.Pasajero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasajeroRepository extends JpaRepository<Pasajero, Long> {
}
