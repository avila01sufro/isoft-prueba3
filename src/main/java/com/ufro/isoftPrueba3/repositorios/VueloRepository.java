package com.ufro.isoftPrueba3.repositorios;

import com.ufro.isoftPrueba3.modelos.Vuelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VueloRepository extends JpaRepository<Vuelo, Long> {
}